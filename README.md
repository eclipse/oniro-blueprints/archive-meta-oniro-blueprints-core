# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

# Introduction

## Description

The `meta-oniro-blueprints-core` is the core layer for Oniro Project blueprints metadata.
The contents of this layer are recipes shared across blueprints but for whatever reason
are inappropriate to be upstreamed to Oniro

## Layer dependencies

The `meta-oniro-blueprints-core` layer depends on the Oniro Project:

https://gitlab.eclipse.org/eclipse/oniro-core/oniro

# Contributing

## Merge requests

All contributions are to be handled as merge requests in the
[oniro blueprints core Gitlab repository](https://gitlab.eclipse.org/eclipse/oniro-core/meta-oniro-blueprints-core). For
more information on the contributing process, check the `CONTRIBUTING.md` file.

## Maintainers

* Eilís Ní Fhlannagáin <elizabeth.flanagan@huawei.com>

# License

This layer is released under the licenses listed in the `LICENSES` root
directory.

